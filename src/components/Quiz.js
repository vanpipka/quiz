import Question from "./Question";
import { useContext } from "react";
import { QuizContext } from "../contexts/quiz";

const Quiz = () => {

  const [quizState, dispatch] = useContext(QuizContext)

  console.log("value: ", quizState);

  return (
    <div className = "quiz">
      {!quizState.showResults && (
        <div>
          <div className = "score">
              Question {quizState.currentQuestionIndex+1}/{quizState.questions.length}
          </div>
          <Question questions={quizState.questions}/>
          <div className = "next-button" onClick={()=> dispatch({type: "NEXT_QUESTION"})}>Next question</div>
        </div>)
      }
      {quizState.showResults && (
        <div className = "next-button" onClick={()=> dispatch({type: "NEW"})}>START AGAIN</div>)
      }
    </div>
  )
}

export default Quiz
