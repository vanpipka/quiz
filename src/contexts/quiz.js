import { createContext } from "react";
import { useReducer } from "react";
import data from "../data";

const initialState = {
  questions: data,
  currentQuestionIndex: 0,
  showResults: false
}

const reducer = (state, action) => {
  //console.log("reducer", state, action);

  if (action.type === "NEXT_QUESTION") {

    if (state.currentQuestionIndex === state.questions.length-1) {
      return {
        ...state,
        showResults: true
      }
    } else {
      return {
        ...state,
        currentQuestionIndex: state.currentQuestionIndex+1
      }
    };
  } else if (action.type === "NEW") {

      return {
        ...state,
        currentQuestionIndex: 0,
        showResults: false
      }
  };

  return state
}

export const QuizContext = createContext();

export const QuizProvider = ({children}) => {

  const value = useReducer(reducer, initialState)

  console.log("state: " , value);

  return <QuizContext.Provider value={value}>{children}</QuizContext.Provider>;
}
